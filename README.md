![SensorSpace Logo](https://sensorspace.tech/images/logo1_vertical.png)

# Flathead Lake Biological Station - Web Server for Plotting Data in Real-Time
In this example we will be setting up a web server from scratch, which will be used to plot sensor data in real-time. The server will retrieve data from The Things Network using telegraf, store the data using InfluxDB, and plot the data using Grafana.

## 1.) Creating Your Web Server
The first thing we need to do is choose a company to host your web server. A popular choice is DigitalOcean - this is the company we will be using for this tutorial. Create an account: https://www.digitalocean.com/

Once you've created an account, click on **Create->Droplets** at the top right of the DigitalOcean website. DigitalOcean calls their servers "Droplets" (it's just a marketing thing...).

![enter image description here](https://i.ibb.co/305w7MN/image.png)

Then click on **Distributions** and click on **Ubuntu 18.04 x64**. For the plan click **Standard** and pick the **$10/month option**.

![enter image description here](https://i.ibb.co/k0PP2FT/image.png)

Choose the location where you want your server to be hosted.

![enter image description here](https://i.ibb.co/KVcNgb4/image.png)

Now give your server a name -- I'm calling mine "zane-test-server". Also assign it to the **flbs-class-2019** project. Click the **Create** button.

![enter image description here](https://i.ibb.co/54hvHPN/image.png)

It will take a minute for your server to be created. Eventually you will see something like this:

![enter image description here](https://i.ibb.co/jM30bjZ/image.png)

Woohoo, your server is online!

## 2.) Updating and Configuring Your Server
We now need to login to our server. To do this, you need an SSH client. A popular one that I'm going to use is called PuTTY. https://www.putty.org/

Open PuTTY and enter the IP address of your newly created server into the **Host Name** box at the top of the program. You can see the IP address of my own server in the image above (142.93.23.105). The click the **Open** button.

![enter image description here](https://i.ibb.co/W6PwSqG/image.png)

A box will popup asking you if you trust this host, click **Yes**. Now you need to enter the credentials of your new server. You can find these credentials in an email that was sent to you from DigitalOcean.

The email I received looked like this:

![enter image description here](https://i.ibb.co/q5njZDk/image.png)

Once you login to your server, it will ask you to change your password. If all goes well, you should see something like this. You can see I'm now logged in as root, on zane-test-server.

![enter image description here](https://i.ibb.co/PtHQ7vg/image.png)

It's bad practice to do anything as the root user in most cases. Let's create a new username for yourself. I'm going to create a user named zane. Enter the following commands:

       adduser zane 			    # create a new user named zane
       usermod -aG sudo zane	            # add the user to the sudo group (gives admin privileges)
       su - zane				    # switch to the new user account

You can see that I'm now logged in as my newly created user named zane.

![enter image description here](https://i.ibb.co/z7MTMRY/image.png)

Let's go ahead and update the server. Enter these three commands. Press enter or type "yes" to anything that pops up during the updates.

    sudo apt-get update             # Fetches the list of available updates
	sudo apt-get upgrade            # Upgrades current packages
	sudo apt-get dist-upgrade       # Installs updates

Now your server is secured and up to date! Next time you login to your server via SSH, always use your username rather than root!

## 3.) Installing InfluxDB

Enter these five commands:

    wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
    source /etc/lsb-release
    echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
    sudo apt-get update && sudo apt-get install influxdb
    sudo systemctl unmask influxdb.service
    sudo ufw allow 8086

Start InfluxDB:

    sudo systemctl start influxdb


We will now start the InfluxDB shell. Type:

    influx

![enter image description here](https://i.ibb.co/xjBDZpZ/image.png)

Before we do anything, InfluxDB requires us to create an admin user (enter your own password in the below example):
    
	CREATE USER admin WITH PASSWORD 'mypassword' WITH ALL PRIVILEGES

Now type:

    exit

We need to modify the InfluxDB configuration file to require password authentication. Open up the file:

    sudo nano /etc/influxdb/influxdb.conf

Scroll down to the **http** section and change **auth-enabled** to **true**, so that it looks like this:

![enter image description here](https://i.ibb.co/h7Gw8V6/image.png)

Now to save your changes, press **Ctrl+x**. Then type "**y**", then press enter. Restart InfluxDB using this command:

    sudo systemctl restart influxdb
Open up InfluxDB as an admin:


	influx -username admin -password mypassword

Let's create a database to store our sensor data in. I'm calling mine "test_db".

    
	CREATE DATABASE test_db
We're all done with InfluxDB, so type exit to leave the InfluxDB shell:
			

    exit

## 4.) Installing Grafana
Enter these five commands:

    wget https://dl.grafana.com/oss/release/grafana_6.2.2_amd64.deb
    sudo apt-get install -y adduser libfontconfig1
    sudo dpkg -i grafana_6.2.2_amd64.deb
    sudo systemctl enable grafana-server.service
    sudo ufw allow 3000

Start Grafana by typing the following (you might have to enter your password):

	systemctl start grafana-server

Grafana should now be running on your server. To check it out, open your web browser and type **http://your_servers_ip_address:3000**

So to access Grafana on my own server, in my web browser I typed **http://142.93.23.105:3000**. The 3000 is needed at the end because Grafana runs on port 3000 by default.

You should see a webpage that looks like this:

![enter image description here](https://i.ibb.co/ts1kjzG/image.png)

The default login credentials are:
Username: **admin**
Password: **admin**

Click the **Log In** button and you will be asked to enter a new password. Afterwards you will be taken to the Grafana dashboard.

![enter image description here](https://i.ibb.co/82nLG1j/image.png)

We need to link Grafana to our InfluxDB database. Open **Configuration->Data Sources**.

![enter image description here](https://i.ibb.co/GtJM4C6/image.png)

Then click on the big green **Add data source** button and then choose **InfluxDB**.

![enter image description here](https://i.ibb.co/VBB3mzG/image.png)

Copy these settings (use your own database name and password, of course).

![enter image description here](https://i.ibb.co/xzVMQMd/image.png)

Scroll down and click on the **Save & Test** button. A green box should pop up, stating that the data source is working.

![enter image description here](https://i.ibb.co/VJQMS4V/image.png)

## 5.) Installing telegraf

Run these commands to install telegraf:

    wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
    source /etc/lsb-release
    echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
    sudo apt-get update && sudo apt-get install telegraf
    sudo systemctl unmask telegraf.service
    
Run this command to start telegraf:

    sudo systemctl start telegraf
    
We need to create a telegraf configuration file:

    sudo rm /etc/telegraf/telegraf.conf
    sudo nano /etc/telegraf/telegraf.conf

Paste this text into the file and save the file. Make sure to use your own things network **application ID** and **application key** for the username and password!

```
[[inputs.mqtt_consumer]]
    servers = ["tcp://us-west.thethings.network:1883"]
    qos = 0
    connection_timeout = "30s"
    topics = [ "+/devices/+/up" ]
    client_id = ""
    username = "sensor-conference"
    password = "ttn-account-v2.Jkgm1aT6KmLIQwrfyQefree0g2yLGH0QyzLhCyF2_Io"
    data_format = "json"

[agent]
  flush_interval = "10s"
  metric_batch_size = 1000

[[outputs.influxdb]]
  urls = ["http://localhost:8086"]
  skip_database_creation = true
  database = "test_db"
  username = "admin"
  password = "mypassword"
```

## 6.) Last minute configuration

Open up the Grafana settings file:

    sudo nano /etc/grafana/grafana.ini

Scroll down to the **auth.anonymous** section and uncomment the three related settings. Make sure to set **enabled = true**. Press Ctrl+x, then "y", then enter to save the changes.

![enter image description here](https://i.ibb.co/0BmMg2Y/image.png)

Reboot the system.

    sudo reboot


## 7.) Let's Make Some Graphs!

Now our server is more or less complete. We can finally use Grafana to generate some cool graphs that plot data live, as it comes in.

In a web browser connect to your server at port 3000. So for me, I'm going to http://142.93.23.105:3000. This will bring up the Grafana dashboard on your server. **Click Create->Dashboard**:

![enter image description here](https://i.ibb.co/zXr8CXz/image.png)

Click on the **Add Query** button.

Create a new chart and copy these settings:

![enter image description here](https://i.ibb.co/SxKRb48/image.png)

Click the save button in the top right.

## 8.) Congrats, you're done!

Here is an example dashboard I made that is plotting temperature in real-time:

http://142.93.23.105:3000/d/lQ-PxH4Zz/temperature-example?orgId=1&refresh=10s&theme=light&from=now-30m&to=now

![enter image description here](https://i.ibb.co/3MdMVnH/image.png)
